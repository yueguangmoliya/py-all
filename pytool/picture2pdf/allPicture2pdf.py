import os
import fitz


def generate():
    path = os.getcwd()
    filename_list = os.listdir(path)
    pic_list = [
        filename for filename in filename_list if filename.endswith((".png", ".PNG", ".jpg", ".JPG"))]
    doc = fitz.open()
    pdf_name = "pic_generate.pdf"
    for pic in pic_list:
        print("插入图片：", pic)
        pic_path = os.path.join(path, pic)
        img_doc = fitz.open(pic_path)
        pdfbytes = img_doc.convertToPDF()
        imgPdf = fitz.open("pdf", pdfbytes)
        doc.insertPDF(imgPdf)
    doc.save(pdf_name)
    print("-------完成--------")
    doc.close()


if __name__ == "__main__":
    generate()
