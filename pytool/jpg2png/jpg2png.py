import os
from PIL import Image


def get_path():
    path = os.getcwd()
    filename_list = os.listdir(path)
    pngdir = os.path.join(path, "png")
    if not os.path.exists(pngdir):
        os.makedirs(pngdir)

    jpgname_list = [
        filename for filename in filename_list if filename.endswith((".jpg", ".jpeg"))]

    for jpgname in jpgname_list:
        pngname = os.path.splitext(jpgname)[0] + ".png"
        # 拼接 路径和文件名
        jpgpath = os.path.join(path, jpgname)
        pngpath = os.path.join(pngdir, pngname)
        # yield 生成器
        yield jpgpath, pngpath


def convert_png():
    for jpgpath, pngpath in get_path():
        try:
            im = Image.open(jpgpath)
            im.save(pngpath)
            print(jpgpath+"-->转换完成")
        except Exception as e:
            print(e)


if __name__ == '__main__':
    convert_png()
