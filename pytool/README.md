## 说明
用python写的日常使用工具

## 1.工具相关依赖
word2pdf(将word转换为pdf)
- word2pdf.py   
	依赖 pip install comtypes 前提本地安装了office 
- wps-word2pdf.py   
	 依赖 pip install pywin32 前提本地安装了wps

---

pdf2picture(将pdf装换为图片)
- pdf2picture.py  
	依赖 pip install PyMuPDF

## 2.打包
---
1.安装pyinstaller  
+ pip install pyinstaller
	
2.切换要打包的目录  
+ pyinstaller -F **.py 
	
3.生成dist目录中的exe文件

4.将exe文件拷到对应位置双击运行