"""
word 转pdf  电脑需要提前安装office
"""
import os
from win32com import client


def get_path():
    path = os.getcwd()
    filename_list = os.listdir(path)
    pdfdir = os.path.join(path, "pdf")
    # 当前目录下生产pdf文件夹
    if not os.path.exists(pdfdir):
        os.makedirs(pdfdir)

    wordname_list = [
        filename for filename in filename_list if filename.endswith((".xls", ".xlsx"))]

    for wordname in wordname_list:
        pdfname = os.path.splitext(wordname)[0] + ".pdf"
        # 拼接 路径和文件名
        wordpath = os.path.join(path, wordname)
        pdfpath = os.path.join(pdfdir, pdfname)
        # yield 生成器
        yield wordpath, pdfpath


def convert_word_to_pdf():
    xl_app = client.DispatchEx("Excel.Application")
    xl_app.Visible = False
    xl_app.DisplayAlerts = 0
    for wordpath, pdfpath in get_path():
        print(">>:", wordpath, "开始转换pdf...")
        books = xl_app.Workbooks.Open(wordpath, False)
        books.ExportAsFixedFormat(0, pdfpath)
        books.Close(False)
        print(">>:", wordpath, "转换完成")
    xl_app.Quit()


if __name__ == "__main__":
    convert_word_to_pdf()
