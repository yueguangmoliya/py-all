import sys

from PIL import Image
import os


def get_pic():
    cmd = os.getcwd()
    all_files = os.listdir(cmd)
    file_list = [f for f in all_files if not os.path.isdir(os.path.join(cmd, f))]
    return file_list


def rotate(angle=90):
    pics = get_pic()
    for pic in pics:
        try:
            img = Image.open(os.path.join(os.getcwd(), pic))
            print(f'--pic-{pic},size={img.size}')
            width = img.size[0]
            height = img.size[1]
            if width > height:
                img = img.rotate(angle, expand=True)
                img.save(pic)
            img.close()
        except:
            try:
                img.close()
            except:
                pass
            pass


if __name__ == "__main__":
    if sys.argv and len(sys.argv) > 1 and sys.argv[1].isdigit():
        rotate(int(sys.argv[1]))
    else:
        rotate()
