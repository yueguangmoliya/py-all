"""
word 转pdf
"""
import fitz
import os


def conver_to_picture():
    path = os.getcwd()
    filename_list = os.listdir(path)

    pdfname_list = [
        filename for filename in filename_list if filename.endswith((".pdf", ".PDF"))]
    print(pdfname_list)
    for pdfname in pdfname_list:
        pdf_to_picture(path, pdfname)
    print("-------全部转换完成-------")


def pdf_to_picture(path, pdfname):
    pdfpath = os.path.join(path, pdfname)
    pdfDoc = fitz.open(pdfpath)
    pdf_pure_name = os.path.splitext(pdfname)[0]
    # imgDir = os.path.join(path, "picture", pdf_pure_name)
    imgDir = os.path.join(path, "picture")
    print("-------pdf %s 开始转换--------" % pdfname)
    for pg in range(pdfDoc.pageCount):
        page = pdfDoc[pg]
        rotate = int(0)
        zoom_x = 2.5
        zoom_y = 2.5
        mat = fitz.Matrix(zoom_x, zoom_y).preRotate(rotate)
        pix = page.getPixmap(matrix=mat, alpha=False)
        # 创建保存的文件夹
        if not os.path.exists(imgDir):
            os.makedirs(imgDir)

        imgpath = os.path.join(imgDir, pdf_pure_name + "-" + str(pg + 1) + ".png")
        pix.writePNG(imgpath)
        print("-------%s 完成--------" % (pdf_pure_name + "-" + str(pg + 1) + ".png"))
    print("-------pdf %s 转换完成--------" % pdfname)


if __name__ == '__main__':
    conver_to_picture()
