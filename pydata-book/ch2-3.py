from datetime import datetime

s = r"this\has\no\special\characters"
print(s)
# 格式化 0-.2float 1-str 2-int
template = "{0:.2f} {1:s} are worth US${2:d}"
s = template.format(88.642, "Argentine Pesos", 12)
print(s)

amount = 10
rate = 88.46
currency = "Pesos"
result = f"{amount} {currency} is worth US${amount / rate}"
print(result)
result = f"{amount} {currency} is worth US${amount / rate:.2f}"
print(result)
# 时间
dt = datetime(2011, 10, 29, 20, 30, 21)
ft = dt.strftime('%Y-%m-%d %H:%M:%S')
print(ft)
print(type(ft))
print(dt.day)
print(dt.minute)
print(dt.date())
print(dt.time())
dat = datetime.strptime('2023-12-15 15:36:12', '%Y-%m-%d %H:%M:%S')
print(dat)
print(type(dat))
dt_hour = dt.replace(minute=0, second=0)
# datatime 不可变
print(dt_hour)
print(dt)
dt2 = datetime(2011, 11, 15, 22, 30)
print(dt2)
# datetime.timedelta
delta = dt2 - dt
print(type(delta))
print(delta)
# 新的datetime
print(dt + delta)
# [5, 4, 3, 2, 1]
print(list(range(5, 0, -1)))
# 元组
tup = 4, 5, 6
print(tup)
# 字典
dt = {"a": 1, "b": 2, "c": 3, "d": 4}
# 删除
v = dt.pop("d")
print(v)
print(dt)
dt.update({"b": 22, "e": 5})
print(dt)
# 通过序列创建map
tuples = zip(range(5), reversed(range(5)))
print(tuples)
mapping = dict(tuples)
print(mapping)
# 按首字母分类
words = ["apple", "bat", "bar", "atom", "book"]
by_letter = {}
for word in words:
    fist_letter = word[0]
    by_letter.setdefault(fist_letter, []).append(word)
print(by_letter)

from collections import defaultdict

by_letter2 = defaultdict(list)
for word in words:
    letter = word[0]
    by_letter2[word[0]].append(word)
print(by_letter2)

# Set
print(set([2, 2, 2, 1, 3, 3]))
s = {2, 2, 2, 1, 3, 3}
print(s)
# 并集、交集..等
a = {1, 2, 3, 4, 5}
b = {3, 4, 5, 6, 7, 8}
# 并集
print(a.union(b))
print(a | b)  # union
# 交集
print(a & b)
print(a.intersection(b))

all_data = [["John", "Emily", "Michael", "Mary", "Steven"],
            ["Maria", "Juan", "Javier", "Natalia", "Pilar"]]

result = [name for names in all_data for name in names if name.count('a') >= 2]
print(result)
# function are object
import re

states = ["   Alabama ", "Georgia!", "Georgia", "georgia", "FlOrIda", "south   carolina##", "West virginia?"]


def remove_punctuation(value):
    return re.sub("[!#?]", "", value)


clean_ops = [str.strip, remove_punctuation, str.title]


def clean_strings(strings, ops):
    result = []
    for value in strings:
        for func in ops:
            value = func(value)
        result.append(value)
    return result


print(clean_strings(states, clean_ops))

import sys
print(sys.getdefaultencoding())

