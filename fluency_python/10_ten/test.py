import functools


#  序列的修改、散列和切片
# 1、Vector类：用户定义的序列类型 @see vectory_v1.py
# 2、Vector类第1版：与Vector2d类兼容
# 3、协议和鸭子类型
# 协议是非正式的，没有强制力，因此如果你知道类的具体使用场景，通常只需要实现一个协议的部分
# 例如，为了支持迭代，只需实现__getitem__ 方法，没必要提供 __len__ 方法
# 4、Vector类第2版：可切片的序列 @see vectory_v2.py
# 切片行为
# 5、Vector类第3版：动态存取属性

class MySeq:
    def __getitem__(self, index):
        return index


s = MySeq()
print(s[1])  # 1
print(s[1:4])  # slice(1, 4, None)
print(s[1:4:2])  # slice(1, 4, 2)
print(s[1:4:2, 9])  # (slice(1, 4, 2), 9)
print(s[1:4:2, 7:9])  # (slice(1, 4, 2), slice(7, 9, None))
print(slice)
print(dir(slice))
# 这个方法会“整顿”元组，把 start、stop 和 stride 都变成非负数，而且都落在指定长度序列的边界内
print(help(slice.indices))
print(slice(None, 10, 2).indices(5))  # (0, 5, 2) 'ABCDE'[:10:2] 等同于 'ABCDE'[0:5:2]
print(slice(-3, None, None).indices(5))  # (2, 5, 1)  'ABCDE'[-3:] 等同于 'ABCDE'[2:5:1]
# 6、Vector类第4版：散列和快速等值测试
# reduce使用
result = functools.reduce(lambda a, b: a * b, range(1, 6))
print(result)
# 10.7 Vector类第5版：格式化
