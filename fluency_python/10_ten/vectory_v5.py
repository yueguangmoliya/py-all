import math
import numbers
from array import array
import reprlib
import functools
import operator
import itertools


class Vector:
    typecode = 'd'
    shortcut_names = 'xyzt'

    def __init__(self, components):
        self._components = array(self.typecode, components)

    def __iter__(self):
        # 迭代
        return iter(self._components)

    def __repr__(self):
        components = reprlib.repr(self._components)  # array('d', [1, 2, 3, 4, 5, ...])
        components = components[components.find('['):-1]  # [1, 2, 3, 4, 5, ...]
        return 'Vector({})'.format(components)

    def __str__(self):
        return str(tuple(self))

    def __bytes__(self):
        return bytes(ord(self.typecode)) + bytes(self._components)

    def __eq__(self, other):
        # 涉及复制 效率不高
        # return tuple(self) == tuple(other)
        # if len(self) != len(other):
        #     return False
        # for a, b in zip(self, other):
        #     # zip 生成一个元组构成的生成器
        #     if a != b:
        #         return False
        # return True
        return len(self) == len(other) and all(a == b for a, b in zip(self, other))

    def __hash__(self):
        hashes = (hash(x) for x in self._components)
        # hashes = map(hash,self._components)
        return functools.reduce(operator.xor, hashes, 0)

    def __abs__(self):
        return math.sqrt(sum(x * x for x in self))

    def __bool__(self):
        return bool(abs(self))

    # __len__ __getitem__ 支持序列操作
    def __len__(self):
        return len(self._components)

    def __getitem__(self, index):
        cls = type(self)
        if isinstance(index, slice):
            return cls(self._components[index])
        elif isinstance(index, numbers.Integral):
            return self._components[index]
        else:
            msg = '{cls.__name__} indices must be integers'
            raise TypeError(msg.format(cls=cls))

    # 属性查找失败后，解释器会调用 __getattr__ 方法
    # 多数时候，如果实现了__getattr__ 方法，那么也要定义__setattr__ 方法
    def __getattr__(self, name):
        cls = type(self)
        if len(name) == 1:
            pos = cls.shortcut_names.find(name)
            if 0 <= pos < len(self._components):
                return self._components[pos]
        msg = '{.__name__!r} object has no attribute {!r}'
        raise AttributeError(msg.format(cls, name))

    def __setattr__(self, name, value):
        cls = type(self)
        if len(name) == 1:
            if name in cls.shortcut_names:
                error = 'readonly attribute {attr_name!r}'
            elif name.islower():
                error = "can't set attribute 'a' to 'z' in {cls_name!r}"
            else:
                error = ''
            if error:
                msg = error.format(cls_name=cls.__name__, attr_name=name)
                raise AttributeError(msg)
        # super() 函数用于动态访问超类的方法
        super().__setattr__(name, value)

    def angle(self, n):
        r = math.sqrt(sum(x * x for x in self[n:]))
        a = math.atan2(r, self[n - 1])
        if (n == len(self) - 1) and self[-1] < 0:
            return math.pi * 2 - a
        else:
            return a

    def angles(self):
        return (self.angle(n) for n in range(1, len(self)))

    def __format__(self, fmt_spec=''):
        if fmt_spec.endswith('h'):  # 超球面坐标
            fmt_spec = fmt_spec[:-1]
            coords = itertools.chain([abs(self)], self.angles())
            outer_fmt = '<{}>'
        else:
            coords = self
            outer_fmt = '({})'
        components = (format(c, fmt_spec) for c in coords)
        return outer_fmt.format(', '.join(components))

    @classmethod
    def frombytes(cls, octets):
        typecode = chr(octets[0])
        memv = memoryview(octets[1:]).cast(typecode)
        return cls(memv)


if __name__ == '__main__':
    print(format(Vector([1, 1]), 'h'))
    print(format(Vector([1, 1]), '.3eh'))
    print(format(Vector([1, 1]), '0.5fh'))
    print(format(Vector([1, 1, 1]), 'h'))
    print(format(Vector([2, 2, 2]), '.3eh'))
    print(format(Vector([0, 0, 0]), '0.5fh'))
    print(format(Vector([-1, -1, -1]), '0.5fh'))
    print(format(Vector([2, 2, 2, 2]), '.3eh'))
    print(format(Vector([0, 1, 0, 0]), '0.5fh'))
