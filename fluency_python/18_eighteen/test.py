# 使用 asyncio 包处理并发
# 并发是指一次处理多件事 并行是指一次做多件事
# asyncio包 使用事件循环驱动的协程实现并发

# 1、线程与协程对比
# @see spinner_asyncio.py spinner_thread.py
# 2、使用asyncio和aiohttp包下载
# @see flags_asyncio.py  python 3.11不能用
# 3、避免阻塞型调用
# 4、改进asyncio下载脚本
# 5、从回调到期物和协程
# 6、使用asyncio包编写服务器
