import contextlib


# contextlib.contextmanager 装饰器会把函数包装成实现 __enter__ 和 __exit__ 方法的类
@contextlib.contextmanager
def looking_glass():
    import sys
    original_write = sys.stdout.write

    def reverse_write(text):
        original_write(text[::-1])

    sys.stdout.write = reverse_write
    yield 'JABBERWOCKY'  # 执行 with 块中的代码时，这个函数会在这一点暂停
    sys.stdout.write = original_write
