from math import hypot


class Vector:

    def __init__(self, x=0, y=0):
        self.x = x
        self.y = y

    def __repr__(self):
        """
        字符串格式
        """
        return 'Vector(%r,%r)' % (self.x, self.y)

    def __abs__(self):
        """
            Vector(3,4) --> 5
        """
        return hypot(self.x, self.y)

    # bool(x) 的背后是调用x.__bool__() 的结果；
    # 如果不存在 __bool__ 方法，那么 bool(x)会尝试调用 x.__len__()，
    # 若返回 0，则 bool 会返回 False；否则返回True。
    def __bool__(self):
        """模为0返回False"""
        return bool(self.x or self.y)
        # return bool(abs(self))

    def __add__(self, other):
        """
         +号
        """
        x = self.x + other.x
        y = self.y + other.y
        return Vector(x, y)

    def __mul__(self, scalar):
        """
            Vector(3,4)*3 --> Vector(9,12)
        """
        return Vector(self.x * scalar, self.y * scalar)


if __name__ == "__main__":
    print(Vector(3, 4))
