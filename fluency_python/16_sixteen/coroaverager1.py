from coroutil import coroutine
from inspect import getgeneratorstate


@coroutine
def averager():
    total = 0
    count = 0
    average = None
    # 仅当调用方在协程上调用 .close() 方法，或者没有对协程的引用而被垃圾回收程序回收时，这个协程才会终止
    while True:
        term = yield average
        total += term
        count += 1
        average = total / count


coro_avg = averager()
print(getgeneratorstate(coro_avg))
print(coro_avg.send(10))
print(coro_avg.send(30))
print(coro_avg.send(5))


