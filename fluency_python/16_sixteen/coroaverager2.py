from collections import namedtuple

Result = namedtuple('Result', 'count average')


def averager():
    total = 0
    count = 0
    average = None
    while True:
        term = yield
        # 为了返回值，协程必须正常终止
        if term is None:
            break
        total += term
        count += 1
        average = total / count

    return Result(count, average)


if __name__ == '__main__':
    coro_avg = averager()
    next(coro_avg)
    coro_avg.send(10)
    coro_avg.send(30)
    coro_avg.send(6.5)
    # StopIteration 异常 value属性保存返回的值
    # print(coro_avg.send(None))
    try:
        coro_avg.send(None)
    except StopIteration as exc:
        result = exc.value
        print(result)
