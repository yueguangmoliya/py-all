import os

fp=open('cafe.txt','w',encoding='utf-8')
print(fp)
fp.write('café')
fp.close()

size=os.stat('cafe.txt').st_size
print(size)

fp2=open('cafe.txt')
print(fp2)
print(fp2.encoding)
print(fp2.read())

fp3=open('cafe.txt',encoding='utf-8')
print(fp3)
print(fp3.read())

fp4=open('cafe.txt','rb') # rb 二进制读取文件 返回BufferedReader
print(fp4)
print(fp4.read())