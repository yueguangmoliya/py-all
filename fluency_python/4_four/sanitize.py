import unicodedata
import string


def shave_marks(txt):
    norm_txt = unicodedata.normalize('NFD', txt)  # 把所有字符分解成基字符和组合记号
    shaved = ''.join(c for c in norm_txt if not unicodedata.combining(c))  # 过滤掉所有组合记号
    return unicodedata.normalize('NFC', shaved)  # 重组所有字符


def shave_marks_latin(txt):
    norm_txt = unicodedata.normalize('NFD', txt)
    latin_base = False
    keepers = []
    for c in norm_txt:
        if unicodedata.combining(c) and latin_base:
            continue
        keepers.append(c)
        if not unicodedata.combining(c):
            latin_base = c in string.ascii_letters
    shaved = ''.join(keepers)
    return unicodedata.normalize('NFC', shaved)


single_map = str.maketrans("""‚ƒ„†ˆ‹‘’“”•–—˜›""",
                           """'f"*^<''""---~>""")
multi_map = str.maketrans({
    '€': '<euro>',
    '…': '...',
    'Œ': 'OE',
    '™': '(TM)',
    'œ': 'oe',
    '‰': '<per mille>',
    '‡': '**',
})

multi_map.update(single_map)


def dewinize(txt):
    return txt.translate(multi_map)


def asciize(txt):
    no_marks = shave_marks_latin(dewinize(txt))
    no_marks = no_marks.replace('ß', 'ss')
    return unicodedata.normalize('NFKC', no_marks)


order = '“Herr Voß: • ½ cup of OEtker™ caffè latte • bowl of açaí.”'
print(shave_marks(order))
print(dewinize(order))
print(asciize(order))
Greek = 'Zέφupoς, Zéfiro'
print(shave_marks(Greek))
