import struct

# < 是小字节序，3s3s 是两个 3 字节序列，HH 是两个16 位二进制整数
fmt = '<3s3sHH'
with open('timg.gif', 'rb') as fp:
    img = memoryview(fp.read())

header = img[:10]
print(bytes(header))
# 拆包 memoryview 对象，得到一个元组，包含类型、版本、宽度和高度
print(struct.unpack(fmt, header))
del header
del img
