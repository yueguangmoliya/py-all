from datetime import datetime


# 符合Python风格的对象

# 1、对象表示形式
# repr() 以便于开发者理解的方式返回对象的字符串表示形式
# str() 以便于用户理解的方式返回对象的字符串表示形式
# 实现 __repr__ 和 __str__特殊方法
# 2、再谈向量类 @see vector2d_v0.py
# 3、备选构造方法 @see vector2d_v1.py
# 4、classmethod与staticmethod
# classmethod 改变了调用方法的方式，类方法的第一个参数是类本身 最常见的用途是定义备选构造方法
# staticmethod 装饰器也会改变方法的调用方式，但是第一个参数不是特殊的值
class Demo:
    @classmethod
    def klassmeth(*args):
        return args

    @staticmethod
    def statmeth(*args):
        return args


print(Demo.klassmeth())
print(Demo.klassmeth('spam'))
print(Demo.statmeth())
print(Demo.statmeth('spam'))
# 5、格式化显示
# 内置的 format() 函数和 str.format() 方法把各个类型的格式化方式委托给相应的 .__format__(format_spec) 方法
# format_spec 格式说明符
brl = 1 / 2.43
print(brl)
print(format(brl, '0.4f'))
print('1 BRL = {rate:0.2f} USD'.format(rate=brl))
# b和x分别表示二进制和十六进制的int类型，f表示小数形式的float类型，而%表示百分数形式
print(format(42, 'b'))
print(format(2 / 3, '.1%'))
now = datetime.now()
print(format(now, '%Y-%m-%d %H:%M:%S'))
print("It's now {:%I:%M %p}".format(now))
# 6、可散列的Vector2d @see vector2d_v3.py
# 7、python的私有属性和“受保护的”属性
# Dog类中__mood属性 -> 实例__dict__中，对象._Dog__mood 名称改写
# 8、使用__slots__类属性节省空间
# 每个子类都要定义 __slots__ 属性，因为解释器会忽略继承的__slots__ 属性
# 实例只能拥有 __slots__ 中列出的属性，除非把 '__dict__' 加入 __slots__ 中（这样做就失去了节省内存的功效）
# 如果不把 '__weakref__' 加入 __slots__，实例就不能作为弱引用的目标
# 如果你的程序不用处理数百万个实例，或许不值得费劲去创建不寻常的类，那就禁止它创建动态属性或者不支持弱引用。与其他优化措施一
# 样，仅当权衡当下的需求并仔细搜集资料后证明确实有必要时，才应该使用 __slots__ 属性
# 大数据的话需要了解Numpy和数据分析库pandas
# 9、覆盖类属性
# 类属性可用于为实例属性提供默认值
# Vector2d 中有个 typecode 类属性，__bytes__ 方法两次用到了它，而且都故意使用 self.typecode 读取它的值。因为 Vector2d 实
# 例本身没有 typecode 属性，所以 self.typecode 默认获取的是Vector2d.typecode 类属性的值
# 假如我们为typecode 实例属性赋值，那么同名类属性不受影响。然而，自此之后，实例读取的 self.typecode 是实例属性 typecode，也就是把同
# 名类属性遮盖了。借助这一特性，可以为各个实例的 typecode 属性定制不同的值。
