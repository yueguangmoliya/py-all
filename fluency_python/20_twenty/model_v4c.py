class Quantity:
    __counter = 0

    def __init__(self):
        cls = self.__class__
        prefix = cls.__name__
        index = cls.__counter
        self.storage_name = '_{}#{}'.format(prefix, index)
        cls.__counter += 1

    # 这里可以使用内置的高阶函数getattr和setattr存取值，无需使用
    # instance.__dict__，因为托管属性和储存属性的名称不同，所以把
    # 储存属性传给getattr函数不会触发描述符

    # 要实现 __get__ 方法，因为托管属性的名称与 storage_name不同
    def __get__(self, instance, owner):
        # owner参数是托管类（如 LineItem）的引用，通过描述符从托管类中获取属性时用得到
        if instance is None:
            # LineItem.weight 调用时
            return self
        else:
            return getattr(instance, self.storage_name)

    def __set__(self, instance, value):
        if value > 0:
            setattr(instance, self.storage_name, value)
        else:
            raise ValueError('value must be > 0')