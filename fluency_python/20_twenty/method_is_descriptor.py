import collections


class Text(collections.UserString):

    def __repr__(self):
        return 'Text({!r})'.format(self.data)

    def reverse(self):
        return self[::-1]


if __name__ == '__main__':
    word = Text('forward')
    print(word)
    print(word.reverse())
    print(Text.reverse(Text('backward')))  # 在类上调用方法相当于调用函数
    print(type(Text.reverse), type(word.reverse))  # 一个是 function，一个是 method
    # Text.reverse 相当于函数，甚至可以处理 Text 实例之外的其他对象
    print(list(map(Text.reverse, ['repaid', (10, 20, 30), Text('stressed')])))
    print(Text.reverse.__get__(word))
    print(Text.reverse.__get__(None, Text))
    print(word.reverse)  # word.reverse 表达式其实会调用Text.reverse.__get__(word)，返回对应的绑定方法
    print(word.reverse.__self__)  # 绑定方法对象有个 __self__ 属性，其值是调用这个方法的实例引用
    print(word.reverse.__func__ is Text.reverse)  # 绑定方法的 __func__ 属性是依附在托管类上那个原始函数的引用
