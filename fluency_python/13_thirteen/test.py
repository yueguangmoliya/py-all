import decimal
from vectory_v6 import Vector

# 正确重载运算符
# 1、运算符重载基础
# 不能重载内置类型的运算符
# 不能新建运算符，只能重载现有的
# is and or not 不能重载
# 2、一元运算符
# x != +x的情况
ctx = decimal.getcontext()
ctx.prec = 40
one_third = decimal.Decimal('1') / decimal.Decimal('3')
print(one_third)
print(one_third == +one_third)
ctx.prec = 28
print(one_third == +one_third)  # False +one_third使用28的精度
print(+one_third)

# 3、重载向量加法运算符+
# 4、重载标量乘法运算符*
# 5、众多比较运算符
# 6、增量赋值运算符
# 不实现__iadd__时，用__add__ a+=b ->a=a+b
# 如果中缀运算符的正向方法（如 __mul__）只处理与 self 属于同一类型的操作数，那就无需实现对应的反向方法（如 __rmul__）
v1 = Vector([1, 2, 3])
v1_alias = v1
print(id(v1))
v1 += Vector([4, 5, 6])
print(v1)
print(id(v1))
print(v1_alias)
print(id(v1_alias))

