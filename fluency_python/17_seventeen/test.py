# 使用期物(future)处理并发
# 1、示例：网络下载的三种风格
# @see flags_threadpool.py flags.py

# 2、阻塞型I/O和GIL(全局解释器锁)
# CPython 解释器本身就不是线程安全的，因此有全局解释器锁（GIL），一次只允许使用一个线程执行 Python 字节码
# 然而，标准库中所有执行阻塞型 I/O 操作的函数，在等待操作系统返回结果时都会释放 GIL。这意味着在 Python 语言这个层次上可以使用多线程
# 而 I/O 密集型 Python 程序能从中受益：一个 Python 线程等待网络响应时，阻塞型 I/O 函数会释放 GIL，再运行一个线程

# 3、使用concurrent.futures模块启动进程
# def download_many(cc_list):
# with futures.ProcessPoolExecutor() as executor:
# 不适合io密集型作业 只适合cpu密集型作业

# 4、实验Executor.map方法
# @see demo_executor_map.py

# 5、显示下载进度并处理错误
import time
from tqdm import tqdm

for i in tqdm(range(1000)):
    time.sleep(.01)
