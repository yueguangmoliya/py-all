class HauntedBus:

    def __init__(self, passengers=[]):
        # 默认值在定义函数时计算（通常在加载模块时）
        self.passengers = passengers

    def pick(self, name):
        self.passengers.append(name)

    def drop(self, name):
        self.passengers.remove(name)


if __name__ == '__main__':
    bus1 = HauntedBus(['Alice', 'Bill'])
    print(bus1.passengers)
    bus1.pick('Charlie')
    bus1.drop('Alice')
    print(bus1.passengers)
    bus2 = HauntedBus()
    bus2.pick('Carrie')
    print(bus2.passengers)
    bus3 = HauntedBus()
    print(bus3.passengers)
    bus3.pick('Dave')
    print(bus2.passengers)
    print(bus3.passengers)
    print(bus2.passengers is bus3.passengers)  # bus2和bus3指向同一个列表
    print(bus1.passengers)
    print(dir(HauntedBus.__init__))
    print(HauntedBus.__init__.__defaults__)
    # bus2.passengers是一个别名，它绑定到HauntedBus.__init__.__defaults__属性的第一个元素上
    print(HauntedBus.__init__.__defaults__[0] is bus2.passengers)