class Cheese:

    def __init__(self, kind):
        self.kind = kind

    def _repr__(self):
        return 'Cheese(%r)' % self.kind
