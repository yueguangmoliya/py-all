class LineItem:

    def __init__(self, description, weight, price):
        self.description = description
        self.weight = weight  # 这里已经使用特性的设值方法
        self.price = price

    def subtotal(self):
        return self.weight * self.price

    @property  # 读值方法
    def weight(self):
        return self.__weight  # 真正的值存储在私有属性 __weight 中

    # 被装饰的读值方法有个 .setter 属性，这个属性也是装饰器
    # 这个装饰器把读值方法和设值方法绑定在一起
    @weight.setter
    def weight(self, value):
        if value > 0:
            self.__weight = value
        else:
            raise ValueError("value must be > 0")


if __name__ == '__main__':
    raisins = LineItem('Golden raisins', 10, 6.95)
    print(raisins.subtotal())
    raisins.weight = -20
    print(raisins.subtotal())
