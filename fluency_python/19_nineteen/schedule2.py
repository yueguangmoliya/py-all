import warnings
import shelve
import osconfeed
import inspect

DB_NAME = 'data/schedule2_db'
CONFERENCE = 'conference.115'


class Record:
    def __init__(self, **kwargs):
        # 使用关键字参数传入的属性构建实例的常用简便方式
        self.__dict__.update(kwargs)

    def __eq__(self, other):
        if isinstance(other, Record):
            return self.__dict__ == other.__dict__
        else:
            return NotImplemented


class MissingDatabaseError(RuntimeError):
    """需要数据库但没有指定数据库时抛出"""


class DBRecord(Record):
    __db = None

    @staticmethod
    def set_db(db):
        DBRecord.__db = db

    @staticmethod
    def get_db():
        return DBRecord.__db

    @classmethod
    def fetch(cls, ident):
        db = cls.get_db()
        try:
            return db[ident]
        except TypeError:
            if db is None:
                msg = "database not set; call '{}.set_db(my_db)'"
                raise MissingDatabaseError(msg.format(cls.__name__))
            else:
                raise

    def __repr__(self):
        if hasattr(self, 'serial'):  # 如果记录有 serial 属性
            cls_name = self.__class__.__name__
            return '<{} serial={!r}>'.format(cls_name, self.serial)
        else:
            return super().__repr__()


class Event(DBRecord):
    @property
    def venue(self):
        key = 'venue.{}'.format(self.venue_serial)
        return self.__class__.fetch(key)

    @property
    def speakers(self):
        if not hasattr(self, '_speaker_objs'):
            spkr_serials = self.__dict__['speakers']
            fetch = self.__class__.fetch
            self._speaker_objs = [fetch('speaker.{}'.format(key)) for key in spkr_serials]
        return self._speaker_objs

    def __repr__(self):
        if hasattr(self, 'name'):
            cls_name = self.__class__.__name__
            return '<{} {!r}>'.format(cls_name, self.name)
        else:
            return super().__repr__()


def load_db(db):
    raw_data = osconfeed.load()
    warnings.warn('loading ' + DB_NAME)
    for collection, rec_list in raw_data['Schedule'].items():
        record_type = collection[:-1]  # 把 'events' 变成 'event'
        cls_name = record_type.capitalize()  # 首字母变成大写
        cls = globals().get(cls_name, DBRecord)
        if inspect.isclass(cls) and issubclass(cls, DBRecord):
            factory = cls
        else:
            factory = DBRecord
        for record in rec_list:
            key = '{}.{}'.format(record_type, record['serial'])
            record['serial'] = key
            db[key] = factory(**record)


if __name__ == '__main__':
    db = shelve.open(DB_NAME)
    # 判断数据库是否填充的简便方法是，检查某个已知的键是否存在；
    # 这里检查的键是 conference.115，即 conference 记录（只有一个）的键
    if CONFERENCE not in db:
        load_db(db)
    DBRecord.set_db(db)
    event = DBRecord.fetch('event.33950')
    print(event)
    print(event.venue)
    print(event.venue.name)
    for spkr in event.speakers:
        print('{0.serial}: {0.name}'.format(spkr))
    db.close()
