import time

DEFAULT_FMT = '[{elapsed:0.8f}s] {name}({args}) -> result'


def clock(fmt=DEFAULT_FMT):
    def decorate(func):
        def clocked(*_args):
            t0 = time.time()
            _result = func(*_args)
            elapsed = time.time() - t0
            name = func.__name__
            args = ",".join(repr(arg) for arg in _args)
            result = repr(_result)
            # **locals() 是为了在 fmt 中引用 clocked 的局部变量
            print(fmt.format(**locals()))
            return _result

        return clocked

    return decorate


if __name__ == '__main__':

    @clock('{name}({args}) dt={elapsed:0.3f}s')
    # @clock('{name}:{elapsed}s')
    # @clock()
    def snooze(seconds):
        time.sleep(seconds)


    for i in range(3):
        snooze(.123)
