# 闭包
def make_averager():
    series = []

    def averager(new_value):
        # series 自由变量 指未在本地作用域中绑定的变量
        series.append(new_value)
        total = sum(series)
        return total / len(series)

    return averager


avg = make_averager()
print(avg(10))
print(avg(11))
print(avg(12))

print(avg.__code__.co_varnames)
print(avg.__code__.co_freevars)  # 自由变量('series',)
# series 的绑定在返回的 avg 函数的 __closure__ 属性中
# avg.__closure__ 中的各个元素对应于avg.__code__.co_freevars 中的一个名称
# 这些元素是 cell 对象，有个 cell_contents 属性，保存着真正的值
print(avg.__closure__)
print(avg.__closure__[0].cell_contents)