# 只记录总值和个数
def make_averager():
    count = 0
    total = 0

    def averager(new_value):
        # count total被赋值 成为局部变量(不是自由变量了) 报错 UnboundLocalError
        # count += 1
        # total += new_value
        nonlocal count, total
        count += 1
        total += new_value
        return total / count

    return averager


avg = make_averager()
print(avg(10))
print(avg(11))
print(avg(12))
