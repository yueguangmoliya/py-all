from clockdeco import clock
import clockdeco2
import time


@clock
def snooze(seconds):
    time.sleep(seconds)


@clock
def factorial(n):
    return 1 if n < 2 else n * factorial(n - 1)


@clockdeco2.clock
def snooze2(seconds):
    time.sleep(seconds)


@clockdeco2.clock
def factorial2(n):
    return 1 if n < 2 else n * factorial(n - 1)


if __name__ == '__main__':
    print('*' * 40, 'Calling snooze(.123)')
    snooze(.123)
    print('*' * 40, 'Calling factorial(6)')
    print('6! = ', factorial(6))
    print(factorial.__name__)
    print('*' * 40, 'Calling snooze2(.123)')
    snooze2(.123)
    print('*' * 40, 'Calling factorial2(6)')
    print('6! = ', factorial2(6))
    print(factorial2.__name__)
