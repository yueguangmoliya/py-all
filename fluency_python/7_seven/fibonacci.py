import functools

from clockdeco import clock


#  可选参数 maxsize=128, typed=False
# maxsize 缓存多少个调用结果,满了之后，旧的结果会被扔掉,最佳性能，maxsize 应该设为 2 的幂
# typed 参数如果设为 True，把不同参数类型得到的结果分开保存，即把通常认为相等的浮点数和整数参数（如 1 和 1.0）区分开
@functools.lru_cache()
@clock
def fibonacci(n):
    if n < 2:
        return n
    return fibonacci(n - 2) + fibonacci(n - 1)


if __name__ == '__main__':
    print(fibonacci(6))
