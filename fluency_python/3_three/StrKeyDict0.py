class StrKeyDict0(dict):

    def __missing__(self, key):
        # 没有判断会无限递归
        if isinstance(key, str):
            raise KeyError(key)
        return self[str(key)]

    def get(self, key, default=None):
        try:
            return self[key]
        except KeyError:
            return default

    def __contains__(self, key):
        # self.keys() 返回的是“视图” 速度快
        return key in self.keys() or str(key) in self.keys()
