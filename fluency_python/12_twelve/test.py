import collections
import numbers
import io
import tkinter

# 继承的优缺点
# 1、子类化内置类型很麻烦
# 内置类型不会调用用户定义的类覆盖的特殊方法
class DoppelDict(dict):
    def __setitem__(self, key, value):
        super().__setitem__(key, [value] * 2)


dd = DoppelDict(one=1)
print(dd)
dd['two'] = 2
print(dd)
dd.update(three=3)  # {'one': 1, 'two': [2, 2], 'three': 3}
print(dd)


# 应继承collections模块中的类 UserDict、UserList、UserString
class DoppelDict2(collections.UserDict):
    def __setitem__(self, key, value):
        super().__setitem__(key, [value] * 2)


dd = DoppelDict2(one=1)
print(dd)
dd['two'] = 2
print(dd)
dd.update(three=3)
print(dd)


# 2、多重继承和方法解析顺序
class A:
    def ping(self):
        print('ping:', self)


class B(A):
    def pong(self):
        print('pong:', self)


class C(A):
    def pong(self):
        print('PONG:', self)


class D(B, C):
    def ping(self):
        super().ping()
        # 直接绕过方法解析 直接指定
        # A.ping(self)
        print('post-ping:', self)

    def pingpong(self):
        self.ping()
        super().ping()
        self.pong()
        super().pong()
        C.pong(self)


d = D()
d.pong()
C.pong(d)
print(D.__mro__)
d.ping()
print("---D pingpong---")
d.pingpong()
# 查看__mro__属性
print(bool.__mro__)


def print_mro(cls):
    print(','.join(c.__name__ for c in cls.__mro__))


print_mro(bool)
print_mro(numbers.Integral)
print_mro(io.BytesIO)
print_mro(io.TextIOWrapper)
print_mro(tkinter.Text)

# 3、多重继承的真实应用
# 4、处理多重继承
# 5、一个现代示例：Django通用视图中的混入
